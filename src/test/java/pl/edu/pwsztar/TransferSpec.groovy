package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class TransferSpec extends Specification{

    @Unroll
    def "should transfer from #sourceAccount to #destinAccount account #cash cash"(){
        given: "initial data"
            def bank = new Bank()
            def accounts = SampleDataGenerator.accountsList
            bank.addSampleAccounts(accounts)
        when: "transfer money"
            def result = bank.transfer(sourceAccount, destinAccount, cash)
        then: "check if transfer is successful"
            result

        where:
        sourceAccount | destinAccount | cash
              1       |       2       | 200
              3       |       1       | 400
              4       |       1       |  10

    }
}
