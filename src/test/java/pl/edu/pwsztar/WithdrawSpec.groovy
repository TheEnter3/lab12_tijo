package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class WithdrawSpec extends Specification{

    @Unroll
    def "should withdraw #cashToWithdraw of #cash from account #accountNumber"(){
        given: "initial data"
            def accounts = SampleDataGenerator.accountsList
            def bank = new Bank()
            bank.addSampleAccounts(accounts)

        when: "withdraw cash"
            def result = bank.withdraw(accountNumber, cashToWithdraw)
        then: "check if it was successful"
            result

        where:
        accountNumber | cash | cashToWithdraw
              1       |  200 |      100
              2       |  0   |      0
              3       |  450 |      200
              4       |  100 |      100
    }
}
