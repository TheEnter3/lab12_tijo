package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;

public class SampleDataGenerator {
    public static List<Account> getAccountsList(){

        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account(1, 200));
        accounts.add(new Account(2, 0));
        accounts.add(new Account(3, 450));
        accounts.add(new Account(4, 100));

        return accounts;
    }
}
